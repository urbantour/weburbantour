package com.example.urbantour;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class listar_lineas extends AppCompatActivity {

    Button resumen_linea1;
    Button resumen_linea2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_lineas);

        resumen_linea1 = (Button)findViewById(R.id.linea1);
        resumen_linea2 = (Button)findViewById(R.id.linea2);



        resumen_linea1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent resumen_linea1 = new Intent(listar_lineas.this, linea1.class);
                startActivity(resumen_linea1);
            }
        });

        resumen_linea2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent resumen_linea2 = new Intent(listar_lineas.this, linea2.class);
                startActivity(resumen_linea2);
            }
        });

    }
}
