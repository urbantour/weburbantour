package com.example.urbantour;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

import com.example.urbantour.activity.SitioInteresActivity;


public class MainActivity extends AppCompatActivity {

    Button boton_lineas;
    Button boton_sitios_interes;
    Button boton_mapa;
    Button boton_mi_ubicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton_lineas = findViewById(R.id.botonLinea);
        boton_sitios_interes = findViewById(R.id.sitios_interes);
        boton_mapa  =  findViewById(R.id.maps);
        boton_mi_ubicacion = findViewById(R.id.ubicacion);





        boton_lineas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent boton_lineas = new Intent(MainActivity.this, listar_lineas.class);
                startActivity(boton_lineas);
            }
        });

        boton_sitios_interes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent boton_sitios_interes = new Intent(MainActivity.this, SitioInteresActivity.class);
                startActivity(boton_sitios_interes);
            }
        });

        boton_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(intent);
            }
        });

        boton_mi_ubicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Mi_ubicacion.class);
                startActivity(intent);
            }
        });
    }
}
