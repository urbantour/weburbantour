package com.example.urbantour.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.urbantour.R;
import com.example.urbantour.model.SitioInteres;


public class SitioInteresActivity extends AppCompatActivity {


    public SitioInteres s1 = new SitioInteres();
    public SitioInteres s2 = new SitioInteres();
    public SitioInteres s3 = new SitioInteres();
    public SitioInteres s4 = new SitioInteres();
    public SitioInteres s5 = new SitioInteres();
    public SitioInteres s6 = new SitioInteres();
    public SitioInteres s7 = new SitioInteres();
    public SitioInteres s8 = new SitioInteres();
    public SitioInteres s9 = new SitioInteres();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitio_interes);
        /*--------------------Datos de sitios-------------*/
        s1.setId(1);
        s2.setId(2);
        s3.setId(3);
        s4.setId(4);
        s5.setId(5);
        s6.setId(6);
        s7.setId(7);
        s8.setId(8);
        s9.setId(9);

        s1.setNombre("Sitio 1");
        s2.setNombre("Sitio 2");
        s3.setNombre("Sitio 3");
        s4.setNombre("Sitio 4");
        s5.setNombre("Sitio 5");
        s6.setNombre("Sitio 6");
        s7.setNombre("Sitio 7");
        s8.setNombre("Sitio 8");
        s9.setNombre("Sitio 9");

    }
/*    public void listar(View v)
    {
        TextView tv = findViewById(R.id.textView1);
        tv.setText(s1.getNombre());
        tv.setText(s2.getNombre());
        tv.setText(s3.getNombre());
        tv.setText(s4.getNombre());
        tv.setText(s5.getNombre());
        tv.setText(s6.getNombre());
        tv.setText(s7.getNombre());
        tv.setText(s8.getNombre());
        tv.setText(s1.getDescripcion());
    }*/
}
